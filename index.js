import express from 'express';
import {Tail} from 'tail';
import { WebClient } from '@slack/web-api';
import configuration from './configuration.json' assert { type: "json" };

const tail = new Tail(configuration.fileToMonitor);
let triggerKeywords = configuration.initialTriggers.map(trigger => trigger.toLowerCase());
let events = [];
let isMonitoring = true;

const slackClient = new WebClient(configuration.slack.token);

tail.on('line', async (line) => {
    if(!isMonitoring){
        return;
    }

    const matchingTriggers = triggerKeywords.filter(keyword => line.toLowerCase().includes(keyword))

    if (matchingTriggers.length === 0) {
        return;
    }

    const timestamp = new Date().toISOString();
    const delivered = false;
    const event = { timestamp, line, matchingTriggers, delivered };
    events.push(event);
});


const deliverEvents = () => {
    const undelivered = events
        .filter(event => event.delivered === false);

    if (undelivered.length > 0){
        sendSlackNotification();
    }

    undelivered.forEach(event => event.delivered = true);
    setTimeout(deliverEvents, configuration.slack.notifications.intervalMillis);
}

const cleanUpEvents = () => {
    while(events.length > configuration.eventBufferCount){
        events.shift();
    }
    setTimeout(cleanUpEvents, 10000);
}

const sendSlackNotification = async () => {
    try {
        await slackClient.chat.postMessage({
            channel: configuration.slack.channel,
            text: configuration.slack.message
        });
    } catch (error) {
        console.error(error);
    }
}

const checkSecret = (req, res, next) => {
    const sentSecret = req.headers['api-secret'];
    if(!sentSecret || sentSecret !== configuration.apiSecret){
        res.sendStatus(401);
        return;
    }
    next();
}

const app = express();
app.use(express.json())
app.all('*', checkSecret)

app.post('/keywords', (req, res) => {
    const keyword = req.body.keyword;

    if (!keyword) {
        res.status(400).send("Required parameter 'keyword' is missing");
        return;
    }

    const lowerCase = keyword.toLowerCase();
    if (triggerKeywords.includes(lowerCase)) {
        res.status(422).send(`Keyword '${lowerCase}' already exists.`);
    } else {
        triggerKeywords.push(lowerCase);
        res.send(`Keyword '${lowerCase}' has been added.`);
    }
});

app.get('/keywords', (req, res) => {
    res.send(triggerKeywords);
});

app.delete('/keywords/:keyword', (req, res) => {
    const keyword = req.params.keyword;
    triggerKeywords = triggerKeywords.filter((k) => k !== keyword);
    res.send(`Keyword '${keyword}' has been deleted.`);
});

app.get('/events', (req, res) => {
    res.send(events);
});

app.delete('/events', (req, res) => {
    events = [];
    res.send('All events have been deleted.');
});

app.put('/status', (req, res) => {
    const monitoring = req.body.monitoring;

    if (monitoring === undefined || typeof monitoring !== "boolean") {
        res.status(400).send("Required parameter 'monitoring' is missing or of wrong datatype");
        return;
    }

    isMonitoring = monitoring;
    res.send(`Monitoring: ${isMonitoring}`)
})

app.get('/status', (req, res) => {
    res.send(`Monitoring: ${isMonitoring}`)
})

app.listen(configuration.port, () => {
    console.log('Server listening on port:', configuration.port);
});

cleanUpEvents();
configuration.slack.notifications.active && deliverEvents();

