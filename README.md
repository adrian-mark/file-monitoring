# File Monitoring

Mockup solution to implement basic monitoring for log files 
and get slack alerts when certain trigger words are written to these files 

# Get started

1. Edit the configuration.json file according to your needs
2. Star the service by running npm run start
3. Use the added Postman collection to play around with the API

## Warning
Not intended to be used in productive settings. 

Known issues include:
- Secrets stored in plain text in config file
- Weak API security with only API secret in place
- Not meant to be used in high load scenarios
- Weak API Design without JSON and proper validation in place